# Your very own resume on Git using JSON Resume and CI/CD


## TL;DR
Too long; didn't read, let's go !

* Check the [repo](https://gitlab.com/afreisinger/afreisinger.gitlab.io) of the project and take a look at the .gitlab-ci.yml file, Dockerfile, and resume.json file. That’s it all.

* Look at the [https://afreisinger.gitlab.io](https://afreisinger.gitlab.io).

## Introduction
Automating things makes us satisfied. From automation of reducing or increasing the temperature of a heater to publishing your resume :D.
It’s very important to have an updated version of the resume, but keeping the editable document is more important. Some folks use traditional ways like having an MS word document, but versioning it and the risk of losing the data is always behind you.
I use GitLab-ci. It’s easy to use, easy to maintain, and easy to set up.
I was looking for a tool which I can edit and publish it easily, Also, It should be more human-readable. Something like JSON or YAML and not the Latex. I found Jsonresume. A great tool that makes a JSON document to a beautiful PDF/HTML resume. We are going to get our resume done using Jsonresume, Gitlab-ci, GitLab pages, and some tricks on Docker.


## Initializing
First, we need to install **“resume-cli”** tool. You need to install npm and node. Search through google or Duckduckgo for the instruction of installing them. Install “resume-cli” using:
```
npm install -g resume-cli
```

Create a directory for storing your resume stuffs .like this:
```
mkdir ~/resume
```

This step is necessary because we need to run this command to initialize our resume.json file.
```
resume init
```

**or** you can also use a **docker** container to **skip installing npm and node**.
_______________________________________________________________________________

```
docker run -it  --user root --workdir /home/pptruser -v $(pwd):/home/pptruser afreisinger/jsonresume:latest resume init
```

Then, enter your name and email address. Note that you can mask your email address for preventing spammer to find that. (e.g mail [AT] domain dot com.)

Remember this file was created as the root user. so you need (may be) to do:

```
sudo chown -R $USER:staff resume.json
```

Now open the **resume.json** and fill the file with your contact information, your skills, your social networks, and your job experiences.

If you only want to see the result of your resume.json, do the following for export to html

```
docker run -it  \
        --user root \
        --workdir /home/pptruser \
        -v $(pwd):/home/pptruser afreisinger/jsonresume:latest resume export resume.html -f html --theme flat
```

or for all themes

```
cd /scripts
./build-page.sh
```

you will be able to see the output in the directory /examples


>
> - *Please note that if you are using docker method, change the permission of resume.json file and chdmod 755 build-page.sh*
>

Now everything is fine, so we can jump to the CI section


## Setup CI
Before the start, I should mention that I created a docker image for resume-cli with most of the official themes. If you are looking for more themes take a look at [this](https://www.npmjs.com/search?q=jsonresume-theme-*).

You can check the [Dockerfile](https://gitlab.com/afreisinger/afreisinger.gitlab.io/-/blob/main/Dockerfile.jsonresume) of it here.

Create a file called .gitlab-ci.yml. Paste this into that file. We will talk about all the parameters.

## Setup Pages
First, We need to explain the “Pages” section, then, we move to generate PDF. Check out this section. We will describe all the stages and lines of it.


```
pages:
  stage: deploy
  image: afreisinger/jsonresume:latest
  
  variables: 
    THEME: "kendall"
    USERNAME: "afreisinger"
    NAME: "Adrian"
    SURNAME: "Freisinger"
    THEME_PDF: "stackoverflow"
  
  script:
    - mkdir public/
    - cp me.jpg public/
    - echo $THEME > public/jsonresume.theme
    - resume export public/index.html -f html --theme $THEME
    - resume export public/$USERNAME.pdf -f pdf --theme $THEME_PDF
    - ls public    
    
  artifacts:
    paths:
      - public
  
  rules:
    - if: '$CI_COMMIT_TAG != null'

  #only:
  #- main
  #- tags
  #when: manual

```

First, we have to create a ***“pages”**** pipeline. This job should have a ***“pages”*** name on it to publish on GitLab pages. If you don’t know what is Gitlab pages search for that. Then we have to specify our lovely docker image. We use afreisinger/jsonresume:latest which I build before. in the stage section, we add ***deploy*** names on it.

In the script section, we create a directory called public which we add the HTML file in it. The next line is the command of building the HTML file. Feel free to change the theme, but before that, check the [jsonresume themes](https://jsonresume.org/themes/).

Job ***artifacts*** are a list of files and directories created by a job once it finishes, So, The next section is for accessing the HTML file.
Wwe set ***"when: manual"*** because we want to do the job manually and from the pipeline page in the repo. You can remove it. Also, I set it to only run in the ***"main"*** branch. You can remove it or change it to your desired branch.

That’s it. Your resume is ready from ***https://yourname.gitlab.io***

But what about having this page on the custom domain? like resume.mydomain.com. So, navigate to Settings > pages > New Domain and add your Domain. Follow the instruction of adding DNS records for your domain provider and wait for verification. Right after the verification, your resume is ready on your custom domain.

>
> - *Please note that if you want to have a public page, Your GitLab repo should be public.*
>

Also, you can set up several Resumes using git branches for several positions or languages. For example a branch for SRE position jobs, a branch for DevOps engineer, and a branch for Software Engineer.


## Setup PDF
Having a PDF file of a resume is a great idea(actually lots of companies want a PDF file of your resume :D) But, it takes a lot of time to fix the issues of resume-cli . Thanks to chromium and puppeteer which resume-cli uses them for generating PDF. But let’s not talk about the result. It’s awful. completely looks not like a resume.
I use a command-line tool called wkhtmltopdf. It’s a popular and powerful tool for converting webpages to PDF. We know that we published our resume webpage using Gitlab pages. In this section, we want to generate a PDF based on that webpage and download it from the artifacts section in the Gitlab pipeline page.
So, let’s move to the GitLab-ci for building PDF

```
pdf:
  stage: artifacts
  image: afreisinger/wkhtmltopdf:latest
    
  script:
    - export NOWTIME=$(date +'%Y_%m_%d')
    - |-
            if [[ ! -d "public/" ]]; then
                mkdir public/
            else
                ls $(pwd)/public 
            fi
    
    - echo $CI_PAGES_URL
    - wkhtmltopdf -s A4 -B 10 -L 10 -R 10 -T 10 --no-background --javascript-delay 6000  $CI_PAGES_URL public/AdrianFreisinger_$NOWTIME.pdf
    - ls $(pwd)/public
  
  rules:
    - if: '$CI_COMMIT_TAG != null'
  
  artifacts:
    paths:
       - $(pwd)/public/*.pdf
  #only:
  #  - main
  # - tags
  #when: manual  
  ```

For image, I created a custom docker image with wkhtmltopdf. You can check the ***Dockerfile*** of it [here](https://gitlab.com/afreisinger/afreisinger.gitlab.io/-/blob/main/Dockerfile.wkhtmltopdf). Also, for more options of ***wkhtmltopdf*** check [here](https://wkhtmltopdf.org/usage/wkhtmltopdf.txt).

In the script section, we set an environment variable for the current time. It’s handy when we want to name files based on the date. The next line is ***wkhtmltopdf*** command which creates a pdf file with a size of A4, with a margin of 10mm on each side of the paper, no need for background, and a 6000ms delay on loading javascript files. Also, we use a Gitlab-CI environment variable($CI_PAGES_UR)L to get the URL of our resume(“pages”).
That’s it. You can download a PDF version of your resume on artifacts. Just take a look at the right side of the pipeline page.
Create a Gitlab repo and add all the files (resume.json and .gitlab-ci.yml) to that. In the script section, we set an environment variable for the current time. It’s handy when we want to name files based on the date. The next line is wkhtmltopdf command which creates a pdf file with a size of A4, with a margin of 10mm on each side of the paper, no need for background, and a 6000ms delay on loading javascript files. Also, we use a Gitlab-CI environment variable($CI_PAGES_UR)L to get the URL of our resume(“pages”).
That’s it. You can download a PDF version of your resume on artifacts. Just take a look at the right side of the pipeline page.
Create a Gitlab repo and add all the files (resume.json and .gitlab-ci.yml) to that.

And this is our complete ***.gitlab-ci.yml*** file.

```
image: docker:latest
services:
  - docker:dind

stages:
#  - build
  - deploy
  - artifacts
  - test

#build:
#   stage: build
#   script:
#     - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
#     - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" .
#     - docker tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" "$CI_REGISTRY_IMAGE:latest"
#     - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" 
#     - docker push "$CI_REGISTRY_IMAGE:latest"
#   only:
#     - master
#   when: manual

pages:
  stage: deploy
  image: afreisinger/jsonresume:latest
  
  variables: 
    THEME: "kendall"
    USERNAME: "afreisinger"
    NAME: "Adrian"
    SURNAME: "Freisinger"
    THEME_PDF: "stackoverflow"
  
  script:
    - mkdir public/
    - cp me.jpg public/
    - echo $THEME > public/jsonresume.theme
    - resume export public/index.html -f html --theme $THEME
    - resume export public/$USERNAME.pdf -f pdf --theme $THEME_PDF
    - ls public    
    
  artifacts:
    paths:
      - public
  
  rules:
    - if: '$CI_COMMIT_TAG != null'

  #only:
  #- main
  #- tags
  #when: manual
              
pdf:
  stage: artifacts
  image: afreisinger/wkhtmltopdf:latest
    
  script:
    - export NOWTIME=$(date +'%Y_%m_%d')
    - |-
            if [[ ! -d "public/" ]]; then
                mkdir public/
            else
                ls $(pwd)/public 
            fi
    
    - echo $CI_PAGES_URL
    - wkhtmltopdf -s A4 -B 10 -L 10 -R 10 -T 10 --no-background --javascript-delay 6000  $CI_PAGES_URL public/AdrianFreisinger_$NOWTIME.pdf
    - ls $(pwd)/public
  
  rules:
    - if: '$CI_COMMIT_TAG != null'
  
  artifacts:
    paths:
       - $(pwd)/public/*.pdf
  #only:
  #  - main
  # - tags
    #when: manual  

unit-test:
    stage: test
    #image: node:latest
    variables: 
        THEME: ""
        USERNAME: "afreisinger"
        NAME: "Adrian"
        SURNAME: "Freisinger"
   
   
    script:
      - echo "The project directory is - $CI_PROJECT_DIR" > unit-test.log
      - echo " The stage is "$CI_JOB_STAGE >> unit-test.log
      - cat $CI_PROJECT_DIR/README.md >> README.log
      - echo "and where am I? - $PWD" >> unit-test.log
      - export NOWTIME=$(date +'%Y_%m_%d') >> unit-test.log
      - echo "The date project is $NOWTIME" >> unit-test.log
      - FILENAME="$NAME$SURNAME"_"${NOWTIME}.pdf"
      - echo "The artefacts are $FILENAME" >> unit-test.log
      - echo "The artefacts public are $USERNAME.pdf" >> unit-test.log
      - echo "The ls /public"
      - |-
            if [[ ! -d "public/" ]]; then
                echo "public/: No such file or directory"
            else
                ls $(pwd)/public >> unit-test.log
            fi         
    rules:
    - if: '$CI_COMMIT_TAG != null'
    
    artifacts:
        paths:
            - "./*.log"
    #only:
    #  - main
    #  - tags
    #when: manual
```


>
> - *Remember that, first you need to run “pages” pipeline then build-pdf.*
> - *Only run CI job, when the commit is tagged. View [Keyword reference for the .gitlab-ci.yaml for more information](https://docs.gitlab.com/ee/ci/yaml/#rules)*
> - *You can look the [pipeline](https://gitlab.com/afreisinger/afreisinger.gitlab.io/-/pipelines) and download the artifacts. 
>


##### [___No rights reserved___](http://creativecommons.org/publicdomain/zero/1.0/) by the author.
 